﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Person> GetTeamPersons(int teamId);
    }
}
