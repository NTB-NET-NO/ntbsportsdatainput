﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Team> GetTeamsByMunicipality(int municipalityId);
        List<OrganizationPerson> GetTeamPersons(int teamId);
    }
}
