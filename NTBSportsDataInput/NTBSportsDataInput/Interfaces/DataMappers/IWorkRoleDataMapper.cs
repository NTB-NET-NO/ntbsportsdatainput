﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IWorkRoleDataMapper
    {
        void DeleteAll();
        WorkRole GetWorkRoleByUserId(Guid userId);
    }
}
