﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetSeasons();
        Season GetSeason(int id);
        Season GetOngoingSeason();
    }
}
