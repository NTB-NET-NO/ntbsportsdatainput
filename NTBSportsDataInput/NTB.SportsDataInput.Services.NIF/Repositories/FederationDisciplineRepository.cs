﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

using log4net;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class FederationDisciplineRepository : IRepository<FederationDiscipline>, IDisposable, IFederationDisciplineDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRepository));

        /// <summary>
        /// The organization service client.
        /// </summary>
        private readonly OrgService1Client _orgServiceClient = new OrgService1Client();

        /// <summary>
        /// The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["NIFServicesUsername"];

        /// <summary>
        /// The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["NIFServicesPassword"];


        public FederationDisciplineRepository()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            if (_orgServiceClient.ClientCredentials != null)
            {
                _orgServiceClient.ClientCredentials.UserName.UserName = _ntbUsername;
                _orgServiceClient.ClientCredentials.UserName.Password = _ntbPassword;
            }
        }


        public int InsertOne(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<FederationDiscipline> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<FederationDiscipline> GetAll()
        {
            throw new NotImplementedException();
        }

        public FederationDiscipline Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<FederationDiscipline> GetFederationDisciplines(int orgId)
        {
            var emptyRequest = new EmptyRequest1();
            var response = _orgServiceClient.GetFederations(emptyRequest);

            if (!response.Success)
            {
                throw new Exception(response.ErrorCode + @": " + response.ErrorMessage);
            }

            var federations = response.Federations;

            var federationDisciplines = new List<FederationDiscipline>();


            var federation = federations.Single(f => f.OrgId == orgId);



            var disciplinesString = federation.Disciplines.Replace("&amp;", "&").Split(';');
            foreach (string disciplinesParts in disciplinesString)
            {
                var discipline = new FederationDiscipline();
                // Splitting this string
                var disciplinesPart = disciplinesParts.Split(':');

                discipline.ActivityId = Convert.ToInt32(disciplinesPart[0]);
                discipline.ActivityCode = Convert.ToInt32(disciplinesPart[1]);
                discipline.ActivityName = disciplinesPart[2];

                federationDisciplines.Add(discipline);
            }

            return federationDisciplines;
        }

        public FederationDiscipline GetFederationDiscipline(int id)
        {
            throw new NotImplementedException();
        }
    }
}
