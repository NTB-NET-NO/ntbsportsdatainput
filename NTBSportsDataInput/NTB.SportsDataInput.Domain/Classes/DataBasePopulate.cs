﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class DataBasePopulate
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}
