﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class UserProfileView
    {
        public List<UserProfile> UserProfiles { get; set; }
        
        public List<WorkRole> WorkRoles { get; set; }
    }
}
