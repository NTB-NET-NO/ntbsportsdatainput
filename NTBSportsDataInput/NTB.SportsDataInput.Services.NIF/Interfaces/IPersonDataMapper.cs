﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface IPersonDataMapper
    {
        Person GetPersonById(int id);
    }
}
