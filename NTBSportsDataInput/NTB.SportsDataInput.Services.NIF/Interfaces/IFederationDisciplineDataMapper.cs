﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface IFederationDisciplineDataMapper
    {
        List<FederationDiscipline> GetFederationDisciplines(int orgId);
        FederationDiscipline GetFederationDiscipline(int id);
    }
}
