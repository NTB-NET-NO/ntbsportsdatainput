﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;
using NTB.SportsDataInput.Services.NIF.Utilities;
using log4net;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class TournamentRepository : IRepository<Tournament>, IDisposable , ITournamentDataMapper
    {
        /// <summary>
        /// The tournament service client.
        /// </summary>
        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        /// <summary>
        /// The organization service client.
        /// </summary>
        private readonly OrgServiceClient _orgServiceClient = new OrgServiceClient();
        
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        public TournamentRepository()
        {
            
            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            if (_orgServiceClient.ClientCredentials == null) return;
            _orgServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            _orgServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
        }
        
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public Tournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     A method to return the tournaments based on municipalities
        /// </summary>
        /// <param name="municipalities">string of ids</param>
        /// <param name="seasonId">season id</param>
        /// <param name="orgId">federation id</param>
        /// <returns>list of tournaments</returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            // todo: Consider using first team based on municipalities, then find out in which tournament this team is involved in

            List<Tournament> distinctTournaments = new List<Tournament>();

            foreach (string municipality in municipalities)
            {
                Logger.Debug("Municipality: " + municipality);

                TournamentRegionsSearchParams tournamentRegionsSearchParams = new TournamentRegionsSearchParams
                    {
                        LocalCouncilIds = municipality,
                        SeasonId = seasonId, 
                        FederationId = orgId
                    };
                TournamentRegionsSearchRequest tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest
                    {
                        SearchParams = tournamentRegionsSearchParams
                    };

                TournamentsResponse searchResult = new TournamentsResponse();
                try
                {
                    searchResult = _tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }

                

                if (!searchResult.Success)
                {
                    return null;
                }

                IEnumerable<Tournament> tournaments = searchResult.Tournaments;

                var tournamentComparer = new TournamentComparer();
                foreach (Tournament tournament in tournaments.Where(tournament => !distinctTournaments.Contains(tournament, tournamentComparer)))
                {
                    distinctTournaments.Add(tournament);
                }
            }
            
            distinctTournaments.Sort((a, b) =>
                {
                    int result = String.Compare(a.TournamentName, b.TournamentName, StringComparison.Ordinal);

                    return result;
                });

            return distinctTournaments;
        }

        public List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId)
        {
            List<Tournament> selectedTournaments = new List<Tournament>();

            foreach (Region region in selectedRegions)
            {
                TournamentRegionsSearchParams tournamentRegionsSearchParams = new TournamentRegionsSearchParams
                {
                    LocalCouncilIds = region.RegionId.ToString(),
                    SeasonId = seasonId
                };
                TournamentRegionsSearchRequest tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest
                {
                    
                    SearchParams = tournamentRegionsSearchParams
                };
                TournamentsResponse searchResult = _tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);

                if (!searchResult.Success) continue;

                foreach (Tournament tournament in searchResult.Tournaments)
                {
                    foreach (ClassCode classCode in ageCategories)
                    {
                        if (classCode.ClassId != tournament.ClassCodeId)
                        {
                            throw new Exception("Age Category not found in database!");
                        }

                        Tournament selectedTournament = new Tournament
                            {
                                TournamentId = tournament.TournamentId,
                                TournamentName = tournament.TournamentName,
                                TournamentNo = tournament.TournamentNo,
                                ClassCodeId = tournament.ClassCodeId
                            };


                        // check if the current tournament is in the selectedtournament list
                        Tournament tournament1 = tournament;
                        var tournaments = from a in selectedTournaments
                                          where
                                              (a.TournamentId == tournament1.TournamentId ||
                                               a.TournamentNo == tournament1.TournamentNo)
                                          select a;

                        if (!tournaments.Any())
                        {
                            selectedTournaments.Add(selectedTournament);
                        }
                    }
                }
            }
            return selectedTournaments;
        }
    }
}
