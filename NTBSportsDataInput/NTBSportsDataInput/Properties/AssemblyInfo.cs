﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using log4net.Config;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NTBSportsDataInput")]
[assembly: AssemblyDescription("Input tool for local sports results")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Norsk Telegrambyrå AS")]
[assembly: AssemblyProduct("NTBSportsDataInput")]
[assembly: AssemblyCopyright("Copyright ©  2014 Norsk Telegrambyrå AS")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: XmlConfigurator(ConfigFile = "web.config", Watch = true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f3182350-d480-4213-96d7-260148a4cfd9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
