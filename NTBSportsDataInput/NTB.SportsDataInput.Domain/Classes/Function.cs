namespace NTB.SportsDataInput.Domain.Classes
{
    public class Function
    {
        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int FunctionTypeId { get; set; }

        public string FunctionTypeName { get; set; }

    }
}
