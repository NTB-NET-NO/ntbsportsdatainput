﻿using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class DefineView
    {
        public DetailsView DetailsView { get; set; }
        public CustomerJob CustomerJob { get; set; }
        public IEnumerable<Season> Seasons { get; set; }
    }
}
