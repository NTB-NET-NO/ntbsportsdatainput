﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class ClassCodeRepository : IRepository<ClassCode>, IDisposable, IClassCodeDataMapper
    {
        /// <summary>
        /// The region service v2 client.
        /// </summary>
        private readonly MatchServiceClient _matchServiceClient = new MatchServiceClient();

        private readonly ActivityService1Client _activityService1Client = new ActivityService1Client();
         
        public ClassCodeRepository()
        {
            if (_matchServiceClient.ClientCredentials == null)
            {
                return;
            }

            if (_activityService1Client.ClientCredentials == null)
            {
                return;
            }

            _matchServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            _matchServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            _activityService1Client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            _activityService1Client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert
        }

        public void Update(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        public int InsertOne(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<ClassCode> domainobject)
        {
            throw new NotImplementedException();
        }

        public List<ClassCode> GetClassCodes(int orgId)
        {
            List<int> genders = new List<int> {(int) Gender.Female, (int) Gender.Male, (int) Gender.Unknown};

            ActivitiesByOrgIdRequest1 orgActivityRequest = new ActivitiesByOrgIdRequest1
            {
                Id = orgId
            };
            
            
            ActivityResponse1 orgActivityResponse = _activityService1Client.GetActivitiesByOrg(orgActivityRequest);

            if (!orgActivityResponse.Success)
            {
                return null;
            }

            List<Activity> activities = orgActivityResponse.Activities.ToList();

            List<ClassCode> classCodes = new List<ClassCode>();
            foreach (Activity activity in activities)
            {
                foreach (int genderId in genders)
                {
                    ClassCodesRequest request = new ClassCodesRequest
                        {
                            FederationId = orgId,
                            Sex = genderId,
                            ActivityId = activity.Id

                        };

                    ClassCodesResponse response = _matchServiceClient.GetClassCodes(request);

                    if (!response.Success)
                    {
                        return null;
                    }
                    classCodes.AddRange(response.ClassCode);
                }
            }
            return classCodes;
        }

        public IQueryable<ClassCode> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Delete(ClassCode domainobject)
        {
            throw new NotImplementedException();
        }

        public ClassCode Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
