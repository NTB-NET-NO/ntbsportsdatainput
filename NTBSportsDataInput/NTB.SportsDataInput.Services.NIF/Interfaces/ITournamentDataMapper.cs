﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId);
        List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId);
    }
}
