using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class FederationDisciplinesMapper : BaseMapper<FederationDiscipline, Domain.Classes.FederationDiscipline>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Domain.Classes.FederationDiscipline> mapper)
        {
            mapper.Relate(x => x.ActivityCode, y=> y.ActivityCode);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
        }
    }
}
