﻿namespace NTB.SportsDataInput.Domain.Classes
{
    public class Player
    {
        public int PlayerId { get; set; }

        public string PlayerName { get; set; }
    }
}
