﻿using System.Linq;
using NTB.SportsDataInput.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace NTBSportsDataInput.Tests
{
    
    
    /// <summary>
    ///This is a test class for BaseUtilitiesTest and is intended
    ///to contain all BaseUtilitiesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BaseUtilitiesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void SplitStringIntoDistrictAndGuid_Test()
        {
            const string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee"; 
            const char splitCharacter = '|';
            Dictionary<int, Guid> expected = new Dictionary<int, Guid>
                {
                    {
                        Convert.ToInt32("1000002"),
                        new Guid("6544009b-bab0-4a41-bfac-1edaea0b54ee")
                    }
                }; 
            Dictionary<int, Guid> actual = BaseUtilities.SplitStringIntoDistrictAndGuid(inputString, splitCharacter);
            Assert.IsTrue(actual.Keys.Count > 0);
        }

        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void CheckDistrictValue_Test()
        {
            const string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee";
            const char splitCharacter = '|';

            var expected = Convert.ToInt32("1000002");
            
            Dictionary<int, Guid> actual = BaseUtilities.SplitStringIntoDistrictAndGuid(inputString, splitCharacter);
            Assert.IsTrue(actual.ContainsKey(expected));
        }

        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void CheckGuidValue_Test()
        {
            const string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee";
            const char splitCharacter = '|';

            var expected = new Guid("6544009b-bab0-4a41-bfac-1edaea0b54ee");

            Dictionary<int, Guid> actual = BaseUtilities.SplitStringIntoDistrictAndGuid(inputString, splitCharacter);
            Assert.IsTrue(actual.ContainsValue(expected));
        }

        /// <summary>
        ///A test for BaseUtilities Constructor
        ///</summary>
        [TestMethod()]
        public void BaseUtilitiesConstructorTest()
        {
            BaseUtilities target = new BaseUtilities();
            
        }

        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void SplitStringIntoListOfUsers_Test()
        {
            const string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee";
            const char splitCharacter = '|';

            var expected = Convert.ToInt32("1000002");

            Dictionary<int, List<Guid>> actual = BaseUtilities.SplitStringIntoListOfUsers(inputString, splitCharacter);
            
            Assert.IsTrue(actual.ContainsKey(expected));
        }

        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void SplitStringIntoListOfUsers_CountUsers_Test()
        {
            const string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee";
            const char splitCharacter = '|';

            var expected = Convert.ToInt32("1000002");

            Dictionary<int, List<Guid>> actual = BaseUtilities.SplitStringIntoListOfUsers(inputString, splitCharacter);

            Assert.IsTrue(actual[expected].Count == 1);
        }

        /// <summary>
        ///A test for SplitStringIntoDistrictAndGuid
        ///</summary>
        [TestMethod()]
        public void SplitStringIntoListOfUsers_CountTwoUsers_Test()
        {
            string inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ee";
            const char splitCharacter = '|';

            var expected = Convert.ToInt32("1000002");

            BaseUtilities.SplitStringIntoListOfUsers(inputString, splitCharacter);

            inputString = "1000002|6544009b-bab0-4a41-bfac-1edaea0b54ef";

            Dictionary<int, List<Guid>> actual = BaseUtilities.SplitStringIntoListOfUsers(inputString, splitCharacter);

            Assert.IsTrue(actual[expected].Count == 2);
        }
    }
}
