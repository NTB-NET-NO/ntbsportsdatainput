﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Utilities
{
    public class TeamComparer : IEqualityComparer<Team>
    {
        public bool Equals(Team x, Team y)
        {
            //Check whether the compared objects reference the same data. 
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            // check if tournamentId are equal
            return x.TeamId == y.TeamId;
        }

        public int GetHashCode(Team obj)
        {
            //Check whether the object is null 
            if (ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Name field if it is not null. 
            int hashProductName = obj.TeamName == null ? 0 : obj.TeamName.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = obj.TeamId.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode; ;
        }
    }
}
