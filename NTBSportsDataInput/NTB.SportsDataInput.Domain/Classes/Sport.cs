// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Sport.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The sport.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The sport.
    /// </summary>
    public class Sport
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the has individual result.
        /// </summary>
        public int? HasIndividualResult { get; set; }

        /// <summary>
        /// Gets or sets the has team results.
        /// </summary>
        public int? HasTeamResults { get; set; }

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int? OrgId { get; set; }

        /// <summary>
        /// Gets or sets the org name.
        /// </summary>
        public string OrgName { get; set; }
    }
}