﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataInput.Services.NFF.Interfaces;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Repositories
{
    public class DistrictRepository : IRepository<District>, IDisposable , IDistrictDataMapper
    {
        /// <summary>
        /// The service client.
        /// </summary>
        public OrganizationServiceClient ServiceClient = new OrganizationServiceClient();

        public int InsertOne(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<District> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<District> GetAll()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new OrganizationServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }

            return ServiceClient.GetDistricts().AsQueryable();
        }

        public District Get(int id)
        {
            return GetAll().Single(s => s.DistrictId == id);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<District> GetDistricts()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new OrganizationServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }

            return ServiceClient.GetDistricts().ToList();
        }

        public District GetDistrict(int districtId)
        {
            return GetAll().Single(s => s.DistrictId == districtId);
        }
    }
}
