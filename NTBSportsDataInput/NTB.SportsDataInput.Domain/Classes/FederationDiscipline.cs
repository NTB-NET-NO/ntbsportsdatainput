namespace NTB.SportsDataInput.Domain.Classes
{
    public class FederationDiscipline
    {
        public int ActivityId { get; set; }
        public int ActivityCode { get; set; }
        public string ActivityName { get; set; }
    }
}