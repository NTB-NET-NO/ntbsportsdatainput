﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NFF.Interfaces;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Repositories
{
    public class MunicipalityRepository : IRepository<Municipality>, IDisposable , IMunicipalityDataMapper
    {
        /// <summary>
        /// The service client.
        /// </summary>
        public MetaServiceClient ServiceClient = new MetaServiceClient();


        public int InsertOne(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Municipality> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> GetAll()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new MetaServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }
            
            return ServiceClient.GetMunicipalities().AsQueryable();
        }

        public Municipality Get(int id)
        {
            return GetAll().Single(s => s.Id == id);
        }

        public List<Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new MetaServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }
            return new List<Municipality>(ServiceClient.GetMunicipalitiesbyDistrict(districtId));
        }

        public void GetFoo()
        {
            ServiceClient.GetAgeCategoriesTournament();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
