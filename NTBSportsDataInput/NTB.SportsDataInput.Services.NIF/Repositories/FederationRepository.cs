﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;
using log4net;
namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class FederationRepository : IRepository<Federation>, IDisposable, IFederationDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRepository));

        /// <summary>
        /// The organization service client.
        /// </summary>
        private readonly OrgService1Client _orgServiceClient = new OrgService1Client();

        /// <summary>
        /// The ntb busername.
        /// </summary>
        private readonly string _ntbUsername = ConfigurationManager.AppSettings["NIFServicesUsername"];

        /// <summary>
        /// The ntb bpassword.
        /// </summary>
        private readonly string _ntbPassword = ConfigurationManager.AppSettings["NIFServicesPassword"];

        public FederationRepository()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            // Setting up the system
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert
            
            if (_orgServiceClient.ClientCredentials != null)
            {
                _orgServiceClient.ClientCredentials.UserName.UserName = _ntbUsername;
                _orgServiceClient.ClientCredentials.UserName.Password = _ntbPassword;
            }
        }

        public int InsertOne(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public Federation GetFederationByOrgId(int orgId)
        {
            var emptyRequest = new EmptyRequest1();
            var response = _orgServiceClient.GetFederations(emptyRequest);

            if (!response.Success)
            {
                throw new Exception(response.ErrorCode + @": " + response.ErrorMessage);
            }

            return response.Federations.Single(e => e.OrgId == orgId);
        }
        
        public void InsertAll(List<Federation> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Federation> GetAll()
        {
            throw new NotImplementedException();
        }

        public Federation Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Federation> GetFederations()
        {
            // Now we must get the federations and find out who has results defined at NIF.
            var emptyRequest = new EmptyRequest1();
            FederationResponse1 federationResponse = _orgServiceClient.GetFederations(emptyRequest);

            if (!federationResponse.Success)
            {
                return null;
            }

            return federationResponse.Federations.ToList();
        }

        public Federation GetFederation(int id)
        {
            throw new NotImplementedException();
        }
    }
}
