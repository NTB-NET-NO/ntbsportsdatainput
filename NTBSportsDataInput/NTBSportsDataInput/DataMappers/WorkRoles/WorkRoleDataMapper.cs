﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using NTB.SportsDataInput.Application.DataMappers.UserProfiles;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.WorkRoles
{
    public class WorkRoleDataMapper :  IRepository<WorkRole>, IWorkRoleDataMapper, IDisposable
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(WorkRoleDataMapper));

        public Guid UserId { get; set; }

        public int InsertOne(WorkRole domainobject)
        {
            // We are actually doing the mapping, not inserting new work roles 

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertUserWorkRoleMap", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", UserId));
                    sqlCommand.Parameters.Add(new SqlParameter("@WorkRoleId", domainobject.Id));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting Work Roles from database");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        public void InsertAll(List<WorkRole> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(WorkRole domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(WorkRole domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<WorkRole> GetAll()
        {
            var workRoles = new List<WorkRole>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetAllWorkRoles", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    
                    while (sqlDataReader.Read())
                    {
                        var workRole = new WorkRole
                            {
                                Id = Convert.ToInt32(sqlDataReader["WorkRoleId"]),
                                Level = Convert.ToInt32(sqlDataReader["WorkRoleLevel"]),
                                Name = sqlDataReader["WorkRoleName"].ToString()
                            };

                        workRoles.Add(workRole);
                        
                    }
                    

                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting Work Roles from database");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return workRoles.AsQueryable();
            }
        }

        public WorkRole Get(int id)
        {
            var workRole = new WorkRole();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetWorkRoleById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (id == 0)
                    {
                        id = 2;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", id));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }


                    while (sqlDataReader.Read())
                    {
                        workRole.Id = Convert.ToInt32(sqlDataReader["WorkRoleId"]);
                        workRole.Level = Convert.ToInt32(sqlDataReader["WorkRoleLevel"]);
                        workRole.Name = sqlDataReader["WorkRoleName"].ToString();
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting Work Roles from database");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return workRole;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            using (
               var sqlConnection =
                   new SqlConnection(
                       ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUserWorkRoles", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting Work Roles from database");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public WorkRole GetWorkRoleByUserId(Guid userId)
        {
            var workRole = new WorkRole();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetWorkRoleByUserId", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        workRole.Id = Convert.ToInt32(sqlDataReader["WorkRoleId"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting Work Roles from database");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return workRole;
        }
    }
}