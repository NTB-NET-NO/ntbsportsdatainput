// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Municipalities.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The municipalities.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The municipalities.
    /// </summary>
    public class Municipality
    {
        /// <summary>
        /// Gets or sets the municipalities id.
        /// </summary>
        public int MunicipalityId { get; set; }

        /// <summary>
        /// Gets or sets the district id.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the municiaplities name.
        /// </summary>
        public string MuniciaplityName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether selected.
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// Gets or sets the Job Id
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        /// Gets or sets the Sports Id
        /// </summary>
        public int SportId { get; set; }
    }
}