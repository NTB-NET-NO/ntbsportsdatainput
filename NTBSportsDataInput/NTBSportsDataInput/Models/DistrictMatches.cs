﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NTB.SportsDataInput.Application.DataMappers.Matches;
using NTB.SportsDataInput.Application.DataMappers.Municipalities;
using NTB.SportsDataInput.Application.DataMappers.Seasons;
using NTB.SportsDataInput.Application.DataMappers.Sports;
using NTB.SportsDataInput.Application.DataMappers.Tournaments;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Facade.NFF.TournamentAccess;
using NTB.SportsDataInput.Facade.NIF.SportAccess;
using log4net;

namespace NTB.SportsDataInput.Application.Models
{
    public class DistrictMatches
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictMatches));

        private static readonly ILog ErrorLogger = LogManager.GetLogger("ThreadAppenderLogger");

        public District District { get; set; }

        public TournamentFacade SoccerFacade { get; set; }

        public Season SportSeason { get; set; }

        public SportFacade SportsFacade { get; set; }

        public bool Done = false;

        private ManualResetEvent _doneEvent = new ManualResetEvent(true);

        public DistrictMatches(ManualResetEvent doneEvent)
        {
            _doneEvent = doneEvent;
        }

        public void GetDistrictMatches()
        {
            string fileContent = "Start GetDistrictMatches with District " + District.DistrictName + ": " + DateTime.Now.ToLongTimeString() + Environment.NewLine;
            
            var tournaments = new List<Tournament>();
            List<Match> matches = new List<Match>();

            var soccerDistrict = new District
                {
                    DistrictId = District.SportId == 16
                                     ? Convert.ToInt32(District.DistrictId)
                                     : Convert.ToInt32(District.SecondaryDistrictId)
                };

            if (soccerDistrict.DistrictId > 0)
            {
                Logger.Info("Getting tournaments for district: " + soccerDistrict.DistrictId + " (" + soccerDistrict.DistrictName +")");
                tournaments.AddRange(SoccerFacade.GetTournamentsByDistrict(soccerDistrict.DistrictId,
                                                                           SportSeason.SeasonId));
            }


            District sportDistrict = new District();
            if (District.SportId != 16)
            {
                sportDistrict = District;
                var municipalityDataMapper = new MunicipalityDataMapper();
                var counties = municipalityDataMapper.GetMunicipalitiesBySportId(23);
                // sportsFacade.GetMunicipalities().Where(x => x.DistrictId == sportDistrict.DistrictId);

                List<string> municipalties =
                    (from county in counties
                     where county.DistrictId == District.DistrictId
                     select county.MunicipalityId.ToString()).ToList();


                var sports = GetTeamSports();

                foreach (var sport in sports.Where(x => x.Id != 16))
                {
                    int seasonId = GetSeason(sport.Id).SeasonId;

                    List<Tournament> foundTournaments = SportsFacade.GetTournamentByMunicipalities(municipalties,
                                                                                                   seasonId,
                                                                                                   Convert.ToInt32(
                                                                                                       sport.OrgId));

                    foreach (Tournament tournament in foundTournaments)
                    {
                        tournament.SportId = sport.Id;

                        tournaments.Add(tournament);
                    }
                    
                }

            }

            foreach (Tournament tournament in tournaments)
            {
                List<Match> foundMatches;
                if (tournament.SportId == 16)
                {
                    fileContent += "Soccer tournamentId: " + tournament.TournamentId + " (" + tournament.TournamentName +
                                   ")" + Environment.NewLine;
                    Logger.Debug("Soccer tournamentid " + tournament.TournamentId + " (" + tournament.TournamentName + ")");
                    foundMatches = SoccerFacade.GetMatchesByTournament(tournament.TournamentId, false, false,
                                                                         false, false)
                                                 .Where(x => x.MatchDate.Date == DateTime.Today.Date).ToList();

                }
                else
                {
                    // Just making sure we have everything under control
                    fileContent += tournament.SportId + " tournamentId: " + tournament.TournamentId + " (" + tournament.TournamentName +
                                   ")" + Environment.NewLine;
                    Logger.Debug("Non-soccer tournament id: " + tournament.TournamentId + " (" + tournament.TournamentName + ")");
                    foundMatches = SportsFacade.GetMatchesByTournamentIdAndSportId(tournament.TournamentId, tournament.SportId).Where(x => x.MatchDate.Date == DateTime.Today.Date).ToList();

                }

                Logger.Debug("Looping over found matches");
                foreach (Match match in foundMatches)
                {
                    
                    match.SportId = tournament.SportId;
                    tournament.DistrictId = District.DistrictId;

                    if (match.SportId == 16)
                    {
                        match.MatchStartTime = Convert.ToInt32(match.MatchDate.ToShortTimeString().Replace(":", ""));
                        
                    }

                    match.DistrictId = District.DistrictId;

                    // Making sure we have a tournament Id on the match object
                    match.TournamentId = tournament.TournamentId;

                    var tournamentRepo = new TournamentDataMapper();

                    Logger.Debug("Checking if we have the tournament in the database");
                    var t = tournamentRepo.Get(match.TournamentId);

                    if (t.TournamentId == 0)
                    {
                        fileContent += "inserting tournament - " + tournament.TournamentName + " into database." + Environment.NewLine;
                        Logger.Info("Inserting tournament " + tournament.TournamentName);
                        tournamentRepo.InsertOne(tournament);
                    }
                    
                    InsertMatch(match);
                    // matches.Add(match);
                }
                    
            }

            fileContent += "inserting into database: " + DateTime.Now.ToLongTimeString()  + Environment.NewLine;
            // We must also check if the tournament is in the database
            
            // InsertMatches(matches);
            fileContent += "Done inserting into database: " + DateTime.Now.ToLongTimeString()  + Environment.NewLine;

            fileContent += "Done with GetDistrictMatches for District: " + District.DistrictName + Environment.NewLine;
            string fileName = DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0') +
                                    DateTime.Today.Day.ToString().PadLeft(2,'0') +
                                    "T" + DateTime.Now.Hour.ToString().PadLeft(2, '0') + 
                                    DateTime.Now.Minute.ToString().PadLeft(2,'0') +
                                    DateTime.Now.Second.ToString().PadLeft(2, '0') +
                                    @"_" + District.DistrictName.Replace(" ", "_") + ".txt";

            List<string> linesToSave = fileContent.Split(Convert.ToChar("\n")).ToList();
            System.IO.File.WriteAllLines(@"C:\Utvikling\SportsDataInput\Test\" + fileName, linesToSave.ToArray());

            _doneEvent.Set();
        }

        private void InsertMatch(Match match)
        {
            var matchDataMapper = new MatchDataMapper();

            int matchId = matchDataMapper.Get(match.MatchId).MatchId;
            Logger.Debug("Checking if match " + match.MatchId + " is in the database");
            if (matchId != 0) return;

            Logger.Debug("Inserting match " + match.MatchId + " into database");
            matchDataMapper.InsertOne(match);
        }

        private void InsertMatches(List<Match> matches)
        {
            var matchDataMapper = new MatchDataMapper();

            foreach (Match match in matches)
            {
                int matchId = matchDataMapper.Get(match.MatchId).MatchId;
                Logger.Debug("Checking if match " + match.MatchId + " is in the database");
                if (matchId == 0)
                {
                    Logger.Debug("Inserting match " + match.MatchId + " into database");
                    matchDataMapper.InsertOne(match);
                }
            }

        }

        private List<Sport> GetTeamSports()
        {
            var sportDataMapper = new SportDataMapper();

            return sportDataMapper.GetAll().Where(x => x.HasTeamResults == 1).ToList();
        }

        private Season GetSeason(int sportId)
        {
            var seasonDataMapper = new SeasonDataMapper();

            var seasons = seasonDataMapper.GetSeasonsBySportId(sportId);

            if (!seasons.Any()) return new Season();

            return sportId != 4 ?
                seasonDataMapper.GetSeasonsBySportId(sportId).Single(x => x.SeasonActive) :
                seasonDataMapper.GetSeasonsBySportId(sportId).First(x => x.SeasonActive);
        }
    }
}