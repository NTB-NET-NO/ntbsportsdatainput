﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class TeamRepository : IRepository<MatchTeam>, IDisposable, ITeamDataMapper
    {
        private readonly OrgServiceClient _serviceClient = new OrgServiceClient();

        public TeamRepository()
        {
            // Setting up the OrganizationClient
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }

        public int InsertOne(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<MatchTeam> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(MatchTeam domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MatchTeam> GetAll()
        {
            throw new NotImplementedException();
        }

        public MatchTeam Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Person> GetTeamPersons(int teamId)
        {
            throw new NotImplementedException();
        }
    }
}
