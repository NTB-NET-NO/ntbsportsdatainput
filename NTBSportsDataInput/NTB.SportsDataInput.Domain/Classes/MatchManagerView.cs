﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class MatchManagerView
    {
        public List<District> Districts { get; set; }

        public List<UserProfile> UserProfiles { get; set; }

        public Dictionary<District, List<Match>> DistrictMatches { get; set; }
    }
}
