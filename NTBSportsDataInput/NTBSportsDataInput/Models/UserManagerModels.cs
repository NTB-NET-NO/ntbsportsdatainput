﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using NTB.SportsDataInput.Application.DataMappers.UserProfiles;
using NTB.SportsDataInput.Application.DataMappers.WorkRoles;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Models
{
    public class UserManagerModels
    {
        public void InsertUser(FormCollection collection)
        {
            var userProfile = new UserProfile
                {
                    UserName = collection["username"].Trim(),
                    Email = collection["email"].Trim(),
                    FirstName = collection["firstname"].Trim(),
                    LastName = collection["lastname"].Trim(),
                    PassWord = collection["password"].Trim()
                };

            var dataMapper = new UserProfileDataMapper();
            dataMapper.InsertOne(userProfile);
        }

        public void UpdateUser(FormCollection collection, Guid userId)
        {
            var userProfile = new UserProfile
            {
                UserName = collection["username"].Trim(),
                Email = collection["email"].Trim(),
                FirstName = collection["firstname"].Trim(),
                LastName = collection["lastname"].Trim(),
                UserId = userId
            };

            // First we check if the user profile is there or not - This is only necessary in cases where we have not added 
            // meta information on the user (like in first settup
            var dataMapper = new UserProfileDataMapper();

            var user = dataMapper.GetUserProfile(userId);

            if (!string.IsNullOrEmpty(user.FirstName))
            {
                dataMapper.Update(userProfile);
                return;
            } 

            // It did not exists, so we insert the profile 
            // TODO: This one will also create the user, so we need to create a new method
            dataMapper.InsertProfile(userProfile);


        }

        public UserProfileView GetUserProfileView()
        {
            var userProfileViews = new UserProfileView();
            
            var profiles = GetUserProfiles();

            var dataMapper = new WorkRoleDataMapper();

            List<WorkRole> workRoles = new List<WorkRole>(dataMapper.GetAll());

            userProfileViews.WorkRoles = workRoles;

            
            List<UserProfile> userProfiles = new List<UserProfile>();
            foreach (UserProfile profile in profiles)
            {
                // We need to check which Work Role this user have (today)
                var id = 2;
                if (profile.WorkRole != null)
                {
                    id = profile.WorkRole.Id;
                }
                var workRole = dataMapper.Get(id);

                UserProfile userProfile = profile;

                userProfile.WorkRole = workRole;

                userProfiles.Add(userProfile);
            }

            userProfileViews.UserProfiles = userProfiles;

            return userProfileViews;
        }

        public List<UserProfile> GetUserProfiles()
        {
            // (from MembershipUser u in Membership.GetAllUsers() select Membership.GetUser(u.UserName, true)).ToList();

            var datamapper = new WorkRoleDataMapper();
            List<UserProfile> userProfiles = new List<UserProfile>();
            foreach (MembershipUser u in Membership.GetAllUsers())
            {
                UserProfile userProfile = new UserProfile();
                
                // Todo: Link the information from the User and userprofile together
                Guid guid = new Guid();
                if (u.ProviderUserKey != null)
                {
                    guid = new Guid(u.ProviderUserKey.ToString());
                }

                var user = GetUserProfile(u.UserName);

                var roles = Roles.GetRolesForUser(u.UserName);

                userProfile.FirstName = user.FirstName;
                userProfile.LastName = user.LastName;
                userProfile.UserId = guid;
                userProfile.UserName = u.UserName;
                userProfile.Email = u.Email;
                userProfile.IsOnline = u.IsOnline;
                userProfile.LastLoginDate = u.LastLoginDate;
                userProfile.Roles = roles;


                userProfile.WorkRole = datamapper.GetWorkRoleByUserId(guid);
                userProfiles.Add(userProfile);
            }

            return userProfiles;
        }


        public UserProfile GetUserProfileByUserId(Guid userId)
        {
            var user = Membership.GetUser(userId);


            if (user == null)
            {
                return new UserProfile();
            }
            else
            {
                var dataUser = new UserProfileDataMapper().GetUserProfile(userId);
                var userProfile = new UserProfile
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    FirstName = dataUser.FirstName,
                    LastName = dataUser.LastName,
                    IsOnline = user.IsOnline,
                    LastLoginDate = user.LastLoginDate,
                    UserId = userId
                };

                return userProfile;
            }
        }

        public UserProfile GetUserProfile(string username)
        {
            var user = Membership.GetUser(username);
            

            if (user == null)
            {
                return new UserProfile();
            }
            else
            {
                var dataUser = new UserProfileDataMapper().GetUserProfile(new Guid(user.ProviderUserKey.ToString()));
                var userProfile = new UserProfile
                    {
                        Email = dataUser.Email,
                        FirstName = dataUser.FirstName,
                        LastName = dataUser.LastName,
                        IsOnline = user.IsOnline,
                        LastLoginDate = user.LastLoginDate
                    };

                return userProfile;
            }
        }
    }
}