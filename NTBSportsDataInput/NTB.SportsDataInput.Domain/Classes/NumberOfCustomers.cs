// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumberOfCustomers.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The number of customers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The number of customers.
    /// </summary>
    public class NumberOfCustomers
    {
        /// <summary>
        /// Gets or sets the activated.
        /// </summary>
        public int Activated { get; set; }

        /// <summary>
        /// Gets or sets the de activated.
        /// </summary>
        public int DeActivated { get; set; }

        /// <summary>
        /// Gets or sets the number of tournaments.
        /// </summary>
        public int NumberOfTournaments { get; set; }
    }
}