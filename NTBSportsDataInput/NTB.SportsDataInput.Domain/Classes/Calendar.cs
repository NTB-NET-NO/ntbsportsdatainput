// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Calendar.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The calendar.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NTB.SportsDataInput.Domain.Interfaces;

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The calendar.
    /// </summary>
    public class Calendar : ICalendar
    {
        /// <summary>
        ///     Gets or sets the calendar id.
        /// </summary>
        public int CalendarId { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the start date time.
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        ///     Gets or sets the end date time.
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        ///     Gets or sets the sport id
        /// </summary>
        public int SportId { get; set; }
    }
}