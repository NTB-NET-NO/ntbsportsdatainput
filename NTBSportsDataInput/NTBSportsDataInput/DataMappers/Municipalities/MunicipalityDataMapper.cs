using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.Municipalities
{
    public class MunicipalityDataMapper : IRepository<Municipality>, IDisposable, IMunicipalityDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalityDataMapper));

        /// <summary>
        ///     Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        public int SportId { get; set; }

        public int InsertOne(Municipality domainobject)
        {
            Logger.Info("About to insert " + domainobject.MuniciaplityName);
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand(); // "", sqlConnection);
                    if (SportId != 16)
                    {
                        sqlCommand.CommandText = "[SportsData_InsertRegionTable]";
                        sqlCommand.Parameters.Add(new SqlParameter("@ParentRegionId", domainobject.DistrictId));
                        sqlCommand.Parameters.Add(new SqlParameter("@RegionId", domainobject.MunicipalityId));
                        sqlCommand.Parameters.Add(new SqlParameter("@RegionName", domainobject.MuniciaplityName));
                    }
                    else
                    {
                        sqlCommand.CommandText = "[SportsData_InsertMunicipalities]";
                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                        sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", domainobject.MunicipalityId));
                        sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityName", domainobject.MuniciaplityName));
                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    }
                    sqlCommand.Connection = sqlConnection;
                    

                    

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();

                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    //int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    sqlCommand.Dispose();

                    Logger.Info(domainobject.MuniciaplityName + " inserted successfully");

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlCommand.Connection.Close();
                    }

                    return 1;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        public void InsertAll(List<Municipality> domainobject)
        {
            foreach (Municipality municipality in domainobject)
            {
                InsertOne(municipality);
            }
        }

        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> GetAll()
        {
            var municipalities = new List<Municipality>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("[SportsData_GetMunicipalityAndDistrictBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                // Adding the sport Id to the parameter
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", SportId));


                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.Read())
                    {
                        var municipality = new Municipality
                            {
                                MunicipalityId = (int) sqlDataReader["MunicipalityId"],
                                DistrictId = (int) sqlDataReader["DistrictId"],
                                MuniciaplityName = sqlDataReader["MunicipalityName"].ToString()
                            };

                        municipalities.Add(municipality);
                    }

                    sqlDataReader.Close();
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            return municipalities.AsQueryable();
        }

        public Municipality Get(int id)
        {
            return GetAll().Single(s => s.MunicipalityId == id);
        }

        /// <summary>
        ///     Gets the selected municipalities based on jobId
        /// </summary>
        /// <param name="jobId">integer</param>
        /// <returns>List of municipalities</returns>
        public List<Municipality> GetMunicipalitiesByJobId(int jobId)
        {
            // [SportsData_GetMunicipalitiesByJobId]
            var municipalities = new List<Municipality>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("[SportsData_GetMunicipalitiesByJobId]", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                // Adding the sport Id to the parameter
                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));


                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        var municipality = new Municipality
                        {
                            MunicipalityId = (int)sqlDataReader["MunicipalityId"],
                            DistrictId = (int)sqlDataReader["DistrictId"],
                            MuniciaplityName = sqlDataReader["MunicipalityName"].ToString()
                        };

                        municipalities.Add(municipality);
                    }

                    sqlDataReader.Close();    
                }

                

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            return municipalities;
        }

        /// <summary>
        ///     Gets the list of municipalities based on sportId
        /// </summary>
        /// <param name="sportId">integer</param>
        /// <returns>Lis tof municipalities</returns>
        public List<Municipality> GetMunicipalitiesBySportId(int sportId)
        {
            var municipalities = new List<Municipality>();

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("[SportsData_GetMunicipalityAndDistrictBySportId]", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                // Adding the sport Id to the parameter
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));


                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var municipality = new Municipality
                            {
                                MunicipalityId = (int) sqlDataReader["MunicipalityId"],
                                DistrictId = (int) sqlDataReader["DistrictId"],
                                MuniciaplityName = sqlDataReader["MunicipalityName"].ToString()
                            };

                        municipalities.Add(municipality);
                    }
                }

                sqlDataReader.Close();
                

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            return municipalities;
        }

        public void StoreMunicipalities(List<Municipality> municipalities)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            if (SportId == 16) return;
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("[SportsData_TruncateRegionsTable]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}