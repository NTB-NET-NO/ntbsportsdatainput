﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Facade.Mappers.NIF;
using NTB.SportsDataInput.Facade.NIF.SportInterface;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.Repositories;

namespace NTB.SportsDataInput.Facade.NIF.SportAccess
{
    public class SportFacade : ISportFacade
    {
        private readonly IFederationDataMapper _federationDataMapper;
        private readonly IFederationDisciplineDataMapper _federationDisciplineDataMapper;
        private readonly ICountyDataMapper _countyDataMapper;
        private readonly IRegionDataMapper _regionDataMapper;
        private readonly IClassCodeDataMapper _classCodeDataMapper;
        private readonly ISeasonDataMapper _seasonDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;
        private readonly IMatchDataMapper _matchDataMapper;
        private readonly IFunctionDataMapper _functionDataMapper;
        private readonly IPersonDataMapper _personDataMapper;

        public SportFacade()
        {
            _federationDataMapper = new FederationRepository();
            _federationDisciplineDataMapper = new FederationDisciplineRepository();
            _countyDataMapper = new CountyRepository();
            _regionDataMapper = new RegionRepository();
            _classCodeDataMapper = new ClassCodeRepository();
            _seasonDataMapper = new SeasonRepository();
            _tournamentDataMapper = new TournamentRepository();
            _matchDataMapper = new MatchRepository();
            _functionDataMapper = new FunctionRepository();
            _personDataMapper = new PersonRepository();

        }
        public List<FederationDiscipline> GetSports()
        {
            throw new NotImplementedException();
        }

        public FederationDiscipline GetSport(int id)
        {
            throw new NotImplementedException();
        }

        public List<Federation> GetFederations()
        {
            var result = _federationDataMapper.GetFederations();

            var mapper = new FederationMapper();

            return result.Select(row => mapper.Map(row, new Federation())).ToList();
        }

        public Federation GetFederation(int id)
        {
            var result = _federationDataMapper.GetFederation(id);

            var mapper = new FederationMapper();

            return mapper.Map(result, new Federation());
        }

        public Federation GetFederationByOrgId(int orgId)
        {
            var result = _federationDataMapper.GetFederationByOrgId(orgId);

            var mapper = new FederationMapper();

            return mapper.Map(result, new Federation());
        }

        public List<FederationDiscipline> GetFederationDisciplines(int orgId)
        {
            var result =  _federationDisciplineDataMapper.GetFederationDisciplines(orgId);

            var mapper = new FederationDisciplinesMapper();

            return result.Select(row => mapper.Map(row, new FederationDiscipline())).ToList();
        }

        public FederationDiscipline GetFederationDiscipline(int id)
        {
            var result = _federationDisciplineDataMapper.GetFederationDiscipline(id);
            
            var mapper = new FederationDisciplinesMapper();

            return mapper.Map(result, new FederationDiscipline());
        }

        public List<District> GetDistricts()
        {
            var mapper = new DistrictMapper();

            var result = _countyDataMapper.GetCounties();

            return result.Select(row => mapper.Map(row, new District())).ToList();
        }

        public List<Municipality> GetMunicipalities()
        {
            var mapper = new MunicipalityMapper();

            var result = _regionDataMapper.GetRegions();

            return result.Select(row => mapper.Map(row, new Municipality())).ToList();

        }

        public List<AgeCategory> GetAgeCategories(int orgId)
        {
            var mapper = new AgeCategoryMapper();

            var result = _classCodeDataMapper.GetClassCodes(orgId);

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        // List<string> municipalities, int seasonId, int orgId
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            var mapper = new TournamentMapper();

            var result = _tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId, orgId);

            if (result == null)
            {
                return new List<Tournament>();
            }

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }

        public List<AgeCategory> GetClassCodes(int orgId)
        {
            GetAgeCategories(orgId);

            return new List<AgeCategory>();
        }

        public List<Season> GetFederationSeasons(int orgId)
        {
            var mapper = new SeasonMapper();

            var result = _seasonDataMapper.GetFederationSeasonByOrgId(orgId);

            return result.Select(row => mapper.Map(row, new Season())).ToList();
        }


        public List<Match> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId)
        {
            var mapper = new MatchMapper();

            var result = _matchDataMapper.GetMatchesByTournamentIdAndSportId(tournamentId, sportId);

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public List<Function> GetTeamFunctions (int teamId, int sportId)
        {
            var mapper = new FunctionMapper();

            var result = _functionDataMapper.GetTeamFunctions(teamId);

            return result.Select(row => mapper.Map(row, new Function())).ToList();
        }

        public ContactPerson GetTeamContact(int personId)
        {
            var mapper = new PersonMapper();

            var result = _personDataMapper.GetPersonById(personId);

            return mapper.Map(result, new ContactPerson());
        }
    }
}
