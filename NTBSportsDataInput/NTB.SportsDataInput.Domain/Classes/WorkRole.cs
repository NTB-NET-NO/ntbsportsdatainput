﻿namespace NTB.SportsDataInput.Domain.Classes
{
    public class WorkRole
    {
        /// <summary>
        ///     Gets or sets the Id of the Work Role
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the level of the work role
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        ///     Gets or sets the name of the work role
        /// </summary>
        public string Name { get; set; }

        
    }
}
