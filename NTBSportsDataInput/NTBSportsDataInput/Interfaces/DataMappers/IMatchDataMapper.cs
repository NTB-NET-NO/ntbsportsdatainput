﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
        List<Match> GetLocalMatchesByDistrictId(int districtId);
        MatchComment GetMatchComment(int matchId);
        void StoreMatchComment(int matchId, string matchComment);
    }
}
