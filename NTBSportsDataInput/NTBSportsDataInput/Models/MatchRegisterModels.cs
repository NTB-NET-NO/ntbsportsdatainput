﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataInput.Application.DataMappers.ContactPersons;
using NTB.SportsDataInput.Application.DataMappers.Districts;
using NTB.SportsDataInput.Application.DataMappers.Matches;
using NTB.SportsDataInput.Application.DataMappers.Sports;
using NTB.SportsDataInput.Application.DataMappers.Teams;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Facade.NFF.TournamentAccess;
using NTB.SportsDataInput.Facade.NIF.SportAccess;
using log4net;

namespace NTB.SportsDataInput.Application.Models
{
    public class MatchRegisterModels
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRegisterModels));

        public MatchRegisterModels()
        {
            // Just an empty constructor
        }

        public string StoreMatchComment(string comment, int matchId)
        {
            var dataMapper = new MatchDataMapper();
            dataMapper.StoreMatchComment(matchId, comment);

            return comment;
        }
        /// <summary>
        ///     Get the matches the logged in user is to register results for
        /// </summary>
        /// <param name="userId">GUID of the current user</param>
        public List<Match> GetUserMatches(Guid userId)
        {
            var districtMapper = new DistrictDataMapper();
            var districts = districtMapper.GetDistrictsByUserId(userId);


            var mapper = new MatchDataMapper();
            var contactMapper = new ContactPersonDataMapper();

            List<Match> matches = new List<Match>();
            foreach (District district in districts)
            {
                var districtMatches = mapper.GetLocalMatchesByDistrictId(district.DistrictId);
                
                // Now we are to get contact persons
                foreach (Match match in districtMatches)
                {
                    match.HomeTeamContactPersons = CheckContacts(contactMapper.GetContactPersonByTeamId(match.HomeTeamId), match.HomeTeamId, match.SportId);
                    
                    match.AwayTeamContactPersons = CheckContacts(contactMapper.GetContactPersonByTeamId(match.AwayTeamId), match.AwayTeamId, match.SportId);
                    match.DistrictName = district.DistrictName;

                    var sportDataMapper = new SportDataMapper();
                    match.SportName = sportDataMapper.Get(match.SportId).Name;
                    if (!matches.Contains(match))
                    {
                        matches.Add(match);
                    }
                }
            }

            return matches.OrderBy(s => s.SportId).ToList();
        }

        private List<ContactPerson> CheckContacts(List<ContactPerson> contacts, int teamId, int sportId)
        {
            var contactMapper = new ContactPersonDataMapper();
            var teamRoleMapper = new TeamRoleDataMapper();

            if (contacts.Count > 0)
            {
                return contacts;
            }
            else
            {
                if (sportId == 16)
                {
                    try
                    {
                        var facade = new TournamentFacade();
                        contacts = facade.GetTeamContacts(teamId);

                        if (contacts != null)
                        {
                            foreach (ContactPerson contact in contacts) {
                            
                                contactMapper.InsertOne(contact);

                                var teamDataMapper = new TeamDataMapper();
                                // Check if team is in database
                                var team = teamDataMapper.Get(contact.TeamId);
                                if (team.TeamId == 0 && team.SportId == 0)
                                {
                                    var myTeam = new Team
                                        {
                                            TeamId = contact.TeamId,
                                            TeamName = contact.TeamName,
                                            SportId = sportId
                                        };
                                    teamDataMapper.InsertOne(myTeam);

                                }

                                teamDataMapper.DeleteTeamContactMap(contact.TeamId, contact.PersonId);
                                teamDataMapper.InsertTeamContactMap(contact.TeamId, contact.PersonId);
                                

                                // Check if Role is in database
                                var teamFunction = teamRoleMapper.GetTeamRoleById(contact.RoleId);
                                if (teamFunction.FunctionTypeId == 0)
                                {
                                    var function = new Function
                                        {
                                            FunctionTypeId = contact.RoleId,
                                            FunctionTypeName = contact.RoleName
                                        };
                                    teamRoleMapper.InsertTeamRole(function);
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                     Logger.Error(exception.Message);
                     Logger.Error(exception.StackTrace);
                    }


                }
                else
                {
                    var facade = new SportFacade();
                    var functions = facade.GetTeamFunctions(teamId, sportId);

                    foreach (Function function in functions)
                    {
                        var contact = facade.GetTeamContact(function.PersonId);

                        contact.TeamId = teamId;
                        contact.RoleId = function.FunctionTypeId;
                        contact.RoleName = function.FunctionTypeName;

                        contacts.Add(contact);
                    }
                }

                foreach (ContactPerson contact in contacts)
                {
                    

                }
                
            }

            return contacts.OrderBy(s => s.PersonId).ToList();
        }
    }
}
