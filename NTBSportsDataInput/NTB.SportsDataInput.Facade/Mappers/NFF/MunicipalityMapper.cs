using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NFF
{
    class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y=> y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MuniciaplityName);
        }
    }
}