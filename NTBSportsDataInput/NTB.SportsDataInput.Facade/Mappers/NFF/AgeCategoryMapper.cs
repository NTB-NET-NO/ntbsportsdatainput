using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NFF
{
    public class AgeCategoryMapper : BaseMapper<AgeCategoryTournament, AgeCategory>
    {
        protected override void SetUpMapper(Mapping<AgeCategoryTournament, AgeCategory> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.CategoryId);
            mapper.Relate(x => x.AgeCategoryName, y => y.CategoryName);
            mapper.Relate(x => x.MinAge, y => y.MinAge);
            mapper.Relate(x => x.MaxAge, y => y.MaxAge);
        }
    }
}