﻿namespace NTB.SportsDataInput.Domain.Classes
{
    public class MatchEvent
    {
        public int MatchEventTypeId { get; set; }
        public string MatchEventType { get; set; }
        public string MatchEventName { get; set; }
        
    }
}
