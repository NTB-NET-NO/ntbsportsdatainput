﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.Teams
{
    public class TeamDataMapper : IRepository<Team>, IDisposable, ITeamDataMapper 
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TeamDataMapper));

        public int InsertOne(Team domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeam", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", domainobject.TeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamName", domainobject.TeamName));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        public void InsertAll(List<Team> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Team> GetAll()
        {
            throw new NotImplementedException();
        }

        public Team Get(int id)
        {
            var team = new Team();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTeamById", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", id));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return team;
                    }

                    while (sqlDataReader.Read())
                    {
                        team.TeamId = id;
                        team.TeamName = sqlDataReader["TeamName"].ToString();
                        team.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return team;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void DeleteTeamContactMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamContactMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertTeamContactMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamContactMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        

        public void InsertTeamRoles(int teamId, int contactId)
        {
            throw new NotImplementedException();
        }
    }
}