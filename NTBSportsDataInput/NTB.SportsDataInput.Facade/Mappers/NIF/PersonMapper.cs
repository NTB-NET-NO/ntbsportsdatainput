﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;
using Person = NTB.SportsDataInput.Services.NIF.NIFProdService.Person;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class PersonMapper : BaseMapper<Person, Domain.Classes.ContactPerson>
    {
        protected override void SetUpMapper(Mapping<Person, Domain.Classes.ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.HomeAddress.PhoneHome, y => y.HomePhone);
            mapper.Relate(x => x.HomeAddress.PhoneMobile, y => y.MobilePhone);
            mapper.Relate(x => x.HomeAddress.PhoneWork, y => y.OfficePhone);
            mapper.Relate(x => x.HomeAddress.Email, y => y.Email);
            
        }
    }
}
