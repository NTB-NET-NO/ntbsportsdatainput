// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Karantener.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The karantener.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The karantener.
    /// </summary>
    public class Karantener
    {
        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public Player PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the yellow card.
        /// </summary>
        public string YellowCard { get; set; }

        /// <summary>
        /// Gets or sets the red card.
        /// </summary>
        public string RedCard { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sanction completed.
        /// </summary>
        public bool SanctionCompleted { get; set; }
    }
}