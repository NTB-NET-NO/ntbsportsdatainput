﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);
        List<Tournament> GetClubsByTournament(int tournamentId, int seasonId);
        List<AgeCategoryTournament> GetAgeCategoriesTournament();
        List<Team> GetTeamsByMunicipality(int municipalityId);
        Tournament GetTournament(int tournamentId);
    }
}
