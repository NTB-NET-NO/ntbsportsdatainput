// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The customer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The customer.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        public int Active { get; set; }

        /// <summary>
        /// Gets or sets the tt customer id.
        /// </summary>
        public int TtCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the media house id.
        /// </summary>
        public int MediaHouseId { get; set; }

        /// <summary>
        ///     Gets or sets the telephone number
        /// </summary>
        public string Telephone { get; set; }
        
    }
}