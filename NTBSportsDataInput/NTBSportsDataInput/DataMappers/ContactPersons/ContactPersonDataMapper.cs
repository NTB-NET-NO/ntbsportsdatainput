﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.ContactPersons
{
    public class ContactPersonDataMapper : IRepository<ContactPerson>, IDisposable, IContactPersonDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ContactPersonDataMapper));

        public int InsertOne(ContactPerson domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertContactPerson", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", domainobject.TeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", domainobject.PersonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));
                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamRoleId", domainobject.RoleId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomePhone", domainobject.HomePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficePhone", domainobject.OfficePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@MobilePhone", domainobject.MobilePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@Active", 1));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        public void InsertAll(List<ContactPerson> domainobject)
        {
            if (!domainobject.Any())
            {
                return;
            }

            foreach (ContactPerson contactPerson in domainobject)
            {
                InsertOne(contactPerson);
            }
        }

        public void Update(ContactPerson domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdateContactPerson", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", domainobject.PersonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", domainobject.FirstName));
                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", domainobject.LastName));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomePhone", domainobject.HomePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficePhone", domainobject.OfficePhone));
                    sqlCommand.Parameters.Add(new SqlParameter("@MobilePhone", domainobject.MobilePhone));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(ContactPerson domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ContactPerson> GetAll()
        {
            throw new NotImplementedException();
        }

        public ContactPerson Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<ContactPerson> GetContactPersonByTeamId(int teamId)
        {
            List<ContactPerson> contactPersons = new List<ContactPerson>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetContactPersonsByTeamId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return contactPersons;
                    }

                    while (sqlDataReader.Read())
                    {
                        var contactPerson = new ContactPerson
                            {
                                PersonId = Convert.ToInt32(sqlDataReader["ContactPersonId"]),
                                FirstName = sqlDataReader["FirstName"].ToString(),
                                LastName = sqlDataReader["LastName"].ToString(),
                                HomePhone = sqlDataReader["HomePhone"].ToString(),
                                OfficePhone = sqlDataReader["OfficePhone"].ToString(),
                                MobilePhone = sqlDataReader["MobilePhone"].ToString(),
                                TeamId = Convert.ToInt32(sqlDataReader["TeamId"]),
                                TeamName = sqlDataReader["TeamName"].ToString(),
                                RoleId = Convert.ToInt32(sqlDataReader["TeamRoleId"]),
                                RoleName = sqlDataReader["TeamRoleName"].ToString(),
                                Active = Convert.ToInt32(sqlDataReader["Active"])
                                
                            };

                        contactPersons.Add(contactPerson);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return contactPersons;

            }
        }

        public void ActivateDeactivateContactPerson(int personId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_ActivateDeactivateContactPerson", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", personId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void DeleteContactTeamMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteTeamContactMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertContactTeamMap(int teamId, int contactId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTeamContactMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamId", teamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ContactPersonId", contactId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}