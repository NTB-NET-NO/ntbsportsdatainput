﻿using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class DetailsView
    {
        public Customer Customer { get; set; }
        public List<Sport> Sports { get; set; }
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
        public List<Job> Jobs { get; set; }
    }
}
