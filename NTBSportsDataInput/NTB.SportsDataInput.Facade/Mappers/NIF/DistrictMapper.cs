﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class DistrictMapper : BaseMapper<Region, District>
    {
        protected override void SetUpMapper(Mapping<Region, District> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionName, y => y.DistrictName);
        }
    }
}
