using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class Team
    {
        public int AgeCategoryId { get; set; }
        public int ClubId { get; set; }
        public int GenderId { get; set; }
        public int TeamId { get; set; }
        public int SportId { get; set; }
        public string TeamName { get; set; }
        public string TeamNameInTournament { get; set; }
        public List<ContactPerson> ContactPersons { get; set; }
        
    }
}