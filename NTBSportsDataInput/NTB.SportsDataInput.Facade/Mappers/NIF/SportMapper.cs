using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class SportMapper : BaseMapper<FederationDiscipline, Sport>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Sport> mapper)
        {
            mapper.Relate(x => x.ActivityName, y=>y.Name);
            mapper.Relate(x => x.ActivityId, y => y.Id);
        }
    }
}


