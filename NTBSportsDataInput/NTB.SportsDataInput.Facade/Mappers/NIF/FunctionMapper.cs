﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;
using Function = NTB.SportsDataInput.Services.NIF.NIFProdService.Function;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class FunctionMapper : BaseMapper<Function, Domain.Classes.Function>
    {
        protected override void SetUpMapper(Mapping<Function, Domain.Classes.Function> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.FunctionTypeId, y => y.FunctionTypeId);
            mapper.Relate(x => x.FunctionTypeName, y => y.FunctionTypeName);
        }
    }
}
