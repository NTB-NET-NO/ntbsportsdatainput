// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentListCustomers.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The tournament list customers.
    /// </summary>
    public class TournamentListCustomers
    {
        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public List<Customer> Customers { get; set; }
    }
}