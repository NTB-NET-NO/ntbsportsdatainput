﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class SeasonRepository : IRepository<Season>, IDisposable, ISeasonDataMapper

    {
        public int InsertOne(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Season> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Season> GetAll()
        {
            throw new NotImplementedException();
        }

        public Season Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        public List<Season> GetFederationSeasonByOrgId(int orgId)
        {
            var serviceClient = new TournamentServiceClient();

            if (serviceClient.ClientCredentials != null)
            {
                serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NIFServicesUsername"];
                serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            //System.Net.ServicePointManager.ServerCertificateValidationCallback =
            //    (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert

            var request = new SeasonRequest
                {
                    OrgId = orgId
                };

            var response = serviceClient.GetFederationSeasons(request);

            serviceClient.Close();

            return response.Success ? new List<Season>(response.Season) : new List<Season>();
        }
    }
}
