﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class MunicipalityMapper : BaseMapper<Region, Municipality>
    {
        protected override void SetUpMapper(Mapping<Region, Municipality> mapper)
        {
            mapper.Relate(x=>x.RegionId, y=>y.MunicipalityId);
            mapper.Relate(x=>x.RegionName, y=>y.MuniciaplityName);
        }
    }
}
