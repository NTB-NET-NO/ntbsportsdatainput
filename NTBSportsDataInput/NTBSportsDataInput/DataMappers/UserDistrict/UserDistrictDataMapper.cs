﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.UserDistrict
{
    public class UserDistrictDataMapper : IRepository<UserDistrictMapper>, IUserDistrictDataMapper, IDisposable
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof (UserDistrictDataMapper));

        public int InsertOne(UserDistrictMapper domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<UserDistrictMapper> domainobject)
        {
            foreach (UserDistrictMapper userDistrictMapper in domainobject)
            {
                InsertUserMap(userDistrictMapper);
            }
        }

        public void Update(UserDistrictMapper domainobject)
        {
            // [SportsDataInput_DeleteUserDistrictMap]
        }

        public void DeleteMappingByDate(DateTime dateTime)
        {
            // Deleting work jobs
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteAllUserDistrictMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", dateTime));


                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(UserDistrictMapper domainobject)
        {
            // Deleting work jobs
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_DeleteUserDistrictMap", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", DateTime.Today.Date));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public IQueryable<UserDistrictMapper> GetAll()
        {
            throw new NotImplementedException();
        }

        public UserDistrictMapper Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void InsertUserMap(UserDistrictMapper domainobject)
        {
            foreach (Guid userId in domainobject.UserIds)
            {
                // now that we have the user, we can insert the rest of the information
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    try
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                        {
                            sqlConnection.Open();
                        }

                        // running the command
                        var sqlCommand = new SqlCommand("SportsDataInput_InsertUserDistrictMap", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                        sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                        sqlCommand.Parameters.Add(new SqlParameter("@WorkDate", DateTime.Today.Date));

                        sqlCommand.ExecuteNonQuery();

                        sqlCommand.Dispose();

                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            Logger.Error(exception.InnerException.Message);
                            Logger.Error(exception.InnerException.StackTrace);
                        }
                    }
                    finally
                    {
                        if (sqlConnection.State == ConnectionState.Open)
                        {
                            sqlConnection.Close();
                        }
                    }
                }
            }
        }

        public bool GetUserByUserIdAndDistrictId(Guid userId, int districtId)
        {
            // now that we have the user, we can insert the rest of the information
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetWorkShiftUserByUserId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", districtId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }
                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return true;
            }
        }

        public List<DateTime> GetAllUserSessionsByUserId(Guid userId)
        {
            // GetAllUserSessionsByUserId
            // now that we have the user, we can insert the rest of the information

            List<DateTime> listOfDates = new List<DateTime>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsDataInput_GetAllUserSessionsByUserId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", userId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return listOfDates;
                    }
                    while (sqlDataReader.Read())
                    {
                        DateTime dateTime = Convert.ToDateTime(sqlDataReader["WorkDate"]);

                        listOfDates.Add(dateTime);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return listOfDates;
            }
        }
    }
}