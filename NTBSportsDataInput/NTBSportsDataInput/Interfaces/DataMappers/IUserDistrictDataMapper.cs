﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IUserDistrictDataMapper
    {
        void InsertUserMap(UserDistrictMapper domainobject);
        bool GetUserByUserIdAndDistrictId(Guid userId, int districtId);
        List<DateTime> GetAllUserSessionsByUserId(Guid userId);
        void DeleteMappingByDate(DateTime dateTime);
    }
}
