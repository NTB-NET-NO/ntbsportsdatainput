﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class AgeCategoryMapper : BaseMapper<ClassCode, AgeCategory>
    {
        protected override void SetUpMapper(Mapping<ClassCode, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.Name, y => y.CategoryName);
            mapper.Relate(x => x.ToAge, y => y.MaxAge);
            mapper.Relate(x => x.FromAge, y => y.MinAge);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
        }
    }
}
