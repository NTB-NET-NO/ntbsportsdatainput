// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobsModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   A job contains information about which sports and districts the customer shall have data from
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// A job contains information about which sports and districts the customer shall have data from
    /// </summary>
    public class JobsModels
    {
        /// <summary>
        /// Gets or sets the municipalities.
        /// </summary>
        public List<Municipality> Municipalities { get; set; }

        /// <summary>
        /// Gets or sets the sports.
        /// </summary>
        public List<Sport> Sports { get; set; }

        /// <summary>
        /// Gets or sets the districts.
        /// </summary>
        public List<District> Districts { get; set; }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public List<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the age category definitions.
        /// </summary>
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
    }
}