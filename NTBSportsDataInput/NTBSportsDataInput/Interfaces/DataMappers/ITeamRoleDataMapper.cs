﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface ITeamRoleDataMapper
    {
        void DeleteTeamRole(int functionId);
        void InsertTeamRole(Function function);
        Function GetTeamRoleById(int functionId);
    }
}
