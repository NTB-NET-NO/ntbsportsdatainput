﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTB.SportsDataInput.Application.DataMappers.ContactPersons;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Models
{
    public class ContactPersonModels
    {
        public ContactPerson UpdateContactPerson(int id, FormCollection collection)
        {
            var dataMapper = new ContactPersonDataMapper();
            var contactPerson = new ContactPerson
                {
                    TeamId = Convert.ToInt32(collection["teamid"]),
                    FirstName = collection["firstname"],
                    PersonId = id,
                    LastName = collection["lastname"],
                    Email = collection["email"],
                    HomePhone = collection["homephone"],
                    OfficePhone = collection["officephone"],
                    MobilePhone = collection["mobilephone"]
                };
            dataMapper.Update(contactPerson);

            return contactPerson;
        }

        public void ChangeContactPersonStatus(int id)
        {
            var dataMapper = new ContactPersonDataMapper();
            dataMapper.ActivateDeactivateContactPerson(id);
        }
    }
}