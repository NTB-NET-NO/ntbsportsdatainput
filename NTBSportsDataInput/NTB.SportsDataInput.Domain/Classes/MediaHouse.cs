// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediaHouse.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The media house.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The media house.
    /// </summary>
    public class MediaHouse
    {
        /// <summary>
        /// Gets or sets the media house id.
        /// </summary>
        public int MediaHouseId { get; set; }

        /// <summary>
        /// Gets or sets the media house name.
        /// </summary>
        public string MediaHouseName { get; set; }
    }
}