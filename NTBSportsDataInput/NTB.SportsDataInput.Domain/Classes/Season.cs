using System;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class Season
    {
        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the season name.
        /// </summary>
        public string SeasonName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether season active.
        /// </summary>
        public bool SeasonActive { get; set; }

        /// <summary>
        /// Gets or sets the season start date.
        /// </summary>
        public DateTime? SeasonStartDate { get; set; }

        /// <summary>
        /// Gets or sets the season end date.
        /// </summary>
        public DateTime? SeasonEndDate { get; set; }

        /// <summary>
        /// Gets or sets the federation discipline id
        /// </summary>
        public int FederationDiscipline { get; set; }

    }
}