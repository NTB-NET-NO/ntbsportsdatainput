﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NFF
{
    class ContactPersonMapper : BaseMapper<OrganizationPerson, Domain.Classes.ContactPerson>
    {
        protected override void SetUpMapper(Mapping<OrganizationPerson, Domain.Classes.ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.SurName, y => y.LastName);
            mapper.Relate(x => x.RoleId, y => y.RoleId);
            mapper.Relate(x => x.RoleName, y => y.RoleName);
            mapper.Relate(x => x.MobilePhone, y => y.MobilePhone);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.Email, y => y.Email);
        }
    }
}
