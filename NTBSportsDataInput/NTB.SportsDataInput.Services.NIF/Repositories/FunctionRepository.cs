﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class FunctionRepository : IRepository<Function>, IDisposable, IFunctionDataMapper
    {
        public int InsertOne(Function domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Function> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Function domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Function domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Function> GetAll()
        {
            throw new NotImplementedException();
        }

        public Function Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Function> GetTeamFunctions(int teamId)
        {
            var client = new FunctionServiceClient();
            if (client.ClientCredentials == null)
            {
                return new List<Function>();
            }

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            FunctionByOrgRequest request = new FunctionByOrgRequest
            {
                OrgId = teamId
            };

            var response = client.GetFunctionsForOrganisation(request);

            if (!response.Success)
            {
                return new List<Function>();
            }

            return response.Functions.ToList();
        }
    }
}
