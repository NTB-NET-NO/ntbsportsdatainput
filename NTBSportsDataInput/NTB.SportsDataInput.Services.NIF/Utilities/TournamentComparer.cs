﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Utilities
{
    internal class TournamentComparer : IEqualityComparer<Tournament>
    {
        public bool Equals(Tournament x, Tournament y)
        {
            //Check whether the compared objects reference the same data. 
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            // check if tournamentId are equal
            return x.TournamentId == y.TournamentId;
        }

        public int GetHashCode(Tournament obj)
        {
            //Check whether the object is null 
            if (ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Name field if it is not null. 
            int hashProductName = obj.TournamentName == null ? 0 : obj.TournamentName.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = obj.TournamentId.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }
    }
}
