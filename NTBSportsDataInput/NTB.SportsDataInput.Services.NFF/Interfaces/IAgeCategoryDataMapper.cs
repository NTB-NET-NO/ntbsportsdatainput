﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Interfaces
{
    public interface IAgeCategoryDataMapper
    {
        List<AgeCategoryTeam> GetAgeCategoriesTeam();
        List<AgeCategoryTournament> GetAgeCategoriesTournament();
    }
}
