﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using NTB.SportsDataInput.Application.Models;
using NTB.SportsDataInput.Domain.Classes;
using O1881.PartnerApi;

namespace NTB.SportsDataInput.Application.Controllers
{
    [Authorize(Roles = "SuperUser,Vaktsjef,Bruker")]
    public class MatchRegisterController : Controller
    {
        //
        // GET: /MatchRegister/

        public ActionResult Index()
        {
            // todo: Need to get the GUID of the logged in user
            // todo: Get the list of matches for today for the district in question
            MembershipUser user = Membership.GetUser(User.Identity.Name);
            Guid guid = new Guid();
            
            if (user != null && user.ProviderUserKey != null)
            {
                guid = new Guid(user.ProviderUserKey.ToString());
            }

            var model = new MatchRegisterModels();

            return View(model.GetUserMatches(guid));
        }

        //
        // GET: /MatchRegister/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /MatchRegister/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchContact(int id, FormCollection collection)
        {
            var listofPersons = new List<Person>();

            var searchString = collection["person"].Replace(' ', '+');
            
            string searchQuery =
                @"https://api.1881bedrift.no/search/search?userName=AndersSivesind&password=nyhe2cop3&msisdn=98843575&query=" +
                searchString + "&level=1&catalogueIds=0|5&pageSize=5";
            var testUri = new Uri(searchQuery);

            HttpWebRequest testrequest = (HttpWebRequest) WebRequest.Create(testUri);
            testrequest.Method = "GET";
            testrequest.ContentLength = 0;

            HttpWebResponse testResponse = (HttpWebResponse) testrequest.GetResponse();
            
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(new NameTable());
            namespaceManager.AddNamespace("empty", "http://1881.no/api/PartnerSearch");
            XDocument doc;
            using (Stream responseStream = testResponse.GetResponseStream())
            {
                
                doc = XDocument.Load(responseStream);
                XNamespace ns = "http://1881.no/api/PartnerSearch";
                

                var resultItems = doc.XPathSelectElements("//empty:ResultItem[empty:ResultType='Person']", namespaceManager);
                foreach (var resultItem in resultItems)
                {
                    var firstname = resultItem.XPathSelectElement("./empty:FirstName", namespaceManager).Value;
                    var lastname = resultItem.XPathSelectElement("./empty:LastName", namespaceManager).Value;
                    var streetAddress = resultItem.XPathSelectElement("./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:Street", namespaceManager).Value 
                        + @" "
                        + resultItem.XPathSelectElement("./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:StreetId", namespaceManager).Value;
                    var city =
                        resultItem.XPathSelectElement("./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:City",
                                                      namespaceManager).Value;
                    var postalCode =
                        resultItem.XPathSelectElement("./empty:Addresses/empty:Address_Search[empty:AddressType='Visiting']/empty:Zip",
                                                      namespaceManager).Value;
                    var mobile =
                        resultItem.XPathSelectElement(
                            "./empty:ContactPoints[empty:ContactPoint_Search/empty:ContactPointType='Mobile']/empty:ContactPoint_Search/empty:Address", namespaceManager)
                                  .Value;

                    var person = new Person
                        {
                            FirstName = firstname,
                            LastName = lastname,
                            PhoneMobile = mobile,
                            City = city,
                            PostalCode = postalCode,
                            StreetAddress = streetAddress
                        };

                    listofPersons.Add(person);
                }

                // /search?userName=<username>&msisdn=<msisdn>&password=<password>&query=<query>&level=0&format=xml
                // https://api.1881bedrift.no/search/search?userName=AndersSivesind&password=nyhe2cop3&msisdn=98843575&query=TEST&level=2&catalogueIds=0|1|5&pageSize=25
            }

            return PartialView(listofPersons);
        }

        //
        // POST: /MatchRegister/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // Post: /MatchRegister/AddComment/MatchId
        [HttpPost]
        public ActionResult AddComment(int id, FormCollection collection)
        {
            // we are to update or insert comment for this match
            string comment = collection["txtBox" + id];
            var model = new MatchRegisterModels();
            var storedComment = model.StoreMatchComment(comment, id);

            return PartialView("AddComment", new MatchComment{
                Comment = storedComment});
        }
        //
        // POST: /MatchRegister/EditContact/5
        [HttpPost]
        public ActionResult EditContact(int id, FormCollection collection)
        {
            var model = new ContactPersonModels();
            var contactPerson = model.UpdateContactPerson(id, collection);

            return Json(contactPerson);
        }

        //
        // Get: /MatchRegister/ChangeStatus/5
        public ActionResult ChangeStatus(int id)
        {
            
            var model = new ContactPersonModels();
            model.ChangeContactPersonStatus(id);

            return RedirectToAction("Index");
        }
        
        //
        // GET: /MatchRegister/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /MatchRegister/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MatchRegister/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MatchRegister/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
