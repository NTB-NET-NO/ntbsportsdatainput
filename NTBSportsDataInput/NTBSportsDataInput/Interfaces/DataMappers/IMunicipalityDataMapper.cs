﻿using System.Collections.Generic;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesByJobId(int jobId);
        List<Municipality> GetMunicipalitiesBySportId(int sportId);
        void StoreMunicipalities(List<Municipality> municipalities);
        void DeleteAll();

    }
}
