// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgeCategory.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The age category.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The age category.
    /// </summary>
    public class AgeCategory
    {
        /// <summary>
        /// Gets or sets the category id.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the category name.
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or sets the minimum age
        /// </summary>
        public int MinAge { get; set; }

        /// <summary>
        /// Gets or sets the maximum age
        /// </summary>
        public int MaxAge { get; set; }

        /// <summary>
        /// Gets or sets the age category definition id
        /// </summary>
        public int AgeCategoryDefinitionId { get; set; }

        /// <summary>
        /// Gets or sets the id for the sport (or discipline, but we don't check for discipline)
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the name for the sport (or discipline, but we don't check for discipline)
        /// </summary>
        public string SportName { get; set; }
    }
}