﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IDistrictDataMapper
    {
        List<District> GetDistrictsByUserId(Guid userId);
        void InsertUserDistrictMap(Guid userId, int districtId);
    }
}
