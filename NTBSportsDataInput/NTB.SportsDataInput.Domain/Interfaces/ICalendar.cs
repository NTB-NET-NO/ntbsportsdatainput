// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICalendar.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The Calendar interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;

namespace NTB.SportsDataInput.Domain.Interfaces
{
    /// <summary>
    /// The Calendar interface.
    /// </summary>
    public interface ICalendar
    {
        // Int values
        /// <summary>
        /// Gets or sets the calendar id.
        /// </summary>
        int CalendarId { get; set; }

        // String Values
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        // DateTime values
        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        DateTime? StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        DateTime? EndDateTime { get; set; }
    }
}