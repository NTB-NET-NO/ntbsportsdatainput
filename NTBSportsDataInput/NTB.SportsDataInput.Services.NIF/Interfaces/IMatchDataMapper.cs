﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface IMatchDataMapper
    {
        List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);
        List<TournamentMatch> GetTodaysMatches(DateTime matchDate, int seasonId);
    }
}
