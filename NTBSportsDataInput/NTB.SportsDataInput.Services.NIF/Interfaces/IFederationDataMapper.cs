﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface IFederationDataMapper
    {
        // Now we must get the federations and find out who has results defined at NIF.
        List<Federation> GetFederations();
        Federation GetFederation(int id);
        Federation GetFederationByOrgId(int orgId);
    }
}
