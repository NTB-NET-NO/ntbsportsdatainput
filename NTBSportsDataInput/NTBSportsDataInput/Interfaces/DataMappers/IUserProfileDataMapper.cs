﻿using System;
using System.Linq;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface IUserProfileDataMapper
    {
        IQueryable<UserProfile> GetUserProfiles();
        UserProfile GetUserProfile(Guid userId);
    }
}
