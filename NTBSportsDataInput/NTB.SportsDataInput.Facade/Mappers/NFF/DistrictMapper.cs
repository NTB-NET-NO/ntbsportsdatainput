using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NFF
{
    public class DistrictMapper : BaseMapper<District, Domain.Classes.District>
    {
        protected override void SetUpMapper(Mapping<District, Domain.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.DistrictName, y => y.DistrictName);
        }
    }
}
