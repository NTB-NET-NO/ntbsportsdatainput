﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using NTB.SportsDataInput.Application.Interfaces.Common;
using NTB.SportsDataInput.Application.Interfaces.DataMappers;
using NTB.SportsDataInput.Domain.Classes;
using log4net;

namespace NTB.SportsDataInput.Application.DataMappers.Populate
{
    public class PopulateDataMapper : IRepository<DataBasePopulate>, IDisposable, IPopulateDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PopulateDataMapper));
        public int InsertOne(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<DataBasePopulate> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(DataBasePopulate domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<DataBasePopulate> GetAll()
        {
            throw new NotImplementedException();
        }

        public DataBasePopulate Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public DataBasePopulate GetDataBaseUpdated()
        {
            var populate = new DataBasePopulate();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetPopulateDateTime", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }


                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["PopulationStart"] != DBNull.Value)
                        {
                            populate.Start = Convert.ToDateTime(sqlDataReader["PopulationStart"]);
                        }

                        if (sqlDataReader["PopulationEnd"] != DBNull.Value)
                        {
                            populate.End = Convert.ToDateTime(sqlDataReader["PopulationEnd"]);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return populate;
            }
        }
    }
}