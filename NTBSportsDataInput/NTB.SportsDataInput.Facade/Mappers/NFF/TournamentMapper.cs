using Glue;
using NTB.SportsDataInput.Common;

namespace NTB.SportsDataInput.Facade.Mappers.NFF
{
    public class TournamentMapper : BaseMapper<Services.NFF.NFFProdService.Tournament, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Services.NFF.NFFProdService.Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y=>y.AgeCategoryId);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNumber, y => y.TournamentNumber);
            mapper.Relate(x => x.PushChanges, y => y.Push); 
            mapper.Relate(x => x.Division, y => y.Division);
            
        }
    }
}
