﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetFederationSeasonByOrgId(int orgId);
    }
}
