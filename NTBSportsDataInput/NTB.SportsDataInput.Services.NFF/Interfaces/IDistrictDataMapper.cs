﻿using System.Collections.Generic;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Interfaces
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistricts();
        District GetDistrict(int districtId);
    }
}
