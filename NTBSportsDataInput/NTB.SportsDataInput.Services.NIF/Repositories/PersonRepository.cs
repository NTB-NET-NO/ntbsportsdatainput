﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Services.NIF.Interfaces;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Repositories
{
    public class PersonRepository: IRepository<Person>, IDisposable, IPersonDataMapper
    {
        public int InsertOne(Person domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Person> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Person domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Person domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Person> GetAll()
        {
            throw new NotImplementedException();
        }

        public Person Get(int id)
        {
            var personClient = new PersonServiceClient();

            if (personClient.ClientCredentials == null)
            {
                return new Person();
            }

            personClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            personClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            var personRequest = new PersonIdRequest2
            {
                Id = id
            };

            var response = personClient.GetPerson(personRequest);
            return !response.Success ? new Person() : response.Person;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Person GetPersonById(int id)
        {
            return Get(id);
        }
    }
}
