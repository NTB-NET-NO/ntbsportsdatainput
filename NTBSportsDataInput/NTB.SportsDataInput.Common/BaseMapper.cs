﻿using Glue;

namespace NTB.SportsDataInput.Common
{
    public abstract class BaseMapper<TLeftType, TRightType>
    {
        private readonly Mapping<TLeftType, TRightType> _map;

        protected BaseMapper()
            : this(null, null)
        {

        }
        protected BaseMapper(System.Func<TRightType, TLeftType> creatorTowardsLeft, System.Func<TLeftType, TRightType> creatorTowardsRight)
        {
            _map = new Mapping<TLeftType, TRightType>(creatorTowardsLeft, creatorTowardsRight);
            SetUpMapper(_map);
        }

        protected abstract void SetUpMapper(Mapping<TLeftType, TRightType> mapper);

        public virtual TRightType Map(TLeftType from, TRightType to)
        {
            return _map.Map(from, to);
        }

        public virtual TLeftType Map(TRightType from, TLeftType to)
        {
            return _map.Map(from, to);
        }

        public Mapping<TLeftType, TRightType> GetMapper()
        {
            return _map;
        }
    }
}