﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NTB.SportsDataInput.Application.DataMappers.Districts;
using NTB.SportsDataInput.Application.DataMappers.Matches;
using NTB.SportsDataInput.Application.DataMappers.Populate;
using NTB.SportsDataInput.Application.DataMappers.UserDistrict;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Facade.NFF.TournamentAccess;
using NTB.SportsDataInput.Facade.NIF.SportAccess;

namespace NTB.SportsDataInput.Application.Models
{
    
    public class MatchManagementModels
    {
        private readonly DistrictMatches _districtMatches;

        public MatchManagementModels()
        {
            
        }

        private Dictionary<District, List<Match>> GetMatchesForDistrict(List<District> districts)
        {
            Dictionary<District, List<Match>> dictionary = new Dictionary<District, List<Match>>();

            
            foreach (District district in districts)
            {
                if (district.DistrictId == 0) continue;
                var mapper = new MatchDataMapper();
                var matches = mapper.GetLocalMatchesByDistrictId(district.DistrictId);

                dictionary.Add(district, matches);
            }

            // Returning the dictionary
            return dictionary;
        }

        public void GetRemoteMatches()
        {
            List<District> districts = GetDistricts();

            GetRemoteMatchesForDistrict(districts);

            // Insert into database
        }

        private void GetRemoteMatchesForDistrict(List<District> districts)
        {
            // first we do the soccer matches
            var soccerFacade = new TournamentFacade();

            // Setting up the sport facade
            var sportsFacade = new SportFacade();

            var soccerSeason = soccerFacade.GetOngoingSeason();

            // Creating a doneEvents Reset Event Array
            ManualResetEvent[] doneEvents = new ManualResetEvent[districts.Count];

            // Creating a counter to help us with creating the threads
            int counter = 0;

            // Creating a list of threads, so we know what each one is doing
            List<Thread> threads = new List<Thread>();

            foreach (District district in districts)
            {
                if (district.DistrictId == 0) continue;

                doneEvents[counter] = new ManualResetEvent(false);

                var districtMatches = new DistrictMatches(doneEvents[counter])
                {
                    District = district,
                    SoccerFacade = soccerFacade,
                    SportSeason = soccerSeason,
                    SportsFacade = sportsFacade
                };

                Thread matchThread = new Thread(districtMatches.GetDistrictMatches)
                    {
                        Name = "districtThread_" + district.DistrictName
                    };
                matchThread.Start();

                threads.Add(matchThread);
                // dictionary.Add(district, matches);

                counter++;
            }

            foreach (ManualResetEvent doneEvent in doneEvents)
            {
                doneEvent.WaitOne();
            }

        }

        public void StoreJobs(FormCollection collection)
        {
            // TODO: Delete all current jobb-mappings.
            var dataMapper = new UserDistrictDataMapper();
            dataMapper.DeleteMappingByDate(DateTime.Today.Date);
            
            string[] jobs = collection["districts[]"].Split(',');

            Dictionary<int, List<Guid>> listofDistrictAndUsers = new Dictionary<int, List<Guid>>();
            BaseUtilities.ListOfDistrictAndUsers = null;

            const char splitChar = '|';
            foreach (string job in jobs)
            {
                listofDistrictAndUsers = BaseUtilities.SplitStringIntoListOfUsers(job, splitChar);
            }

            // now that we have this list of districts and users, we can start updating the match table
            var objUserDistrictMapper = new List<UserDistrictMapper>();
            

            foreach (KeyValuePair<int, List<Guid>> kvp in listofDistrictAndUsers)
            {
                
                var district = kvp.Key;
                var users = kvp.Value;
                var listOfUsers = new List<Guid>();
                foreach (Guid user in users)
                {
                    
                    var returnval = dataMapper.GetUserByUserIdAndDistrictId(user, district);

                    if (returnval == false)
                    {
                        listOfUsers.Add(user);
                    }
                }

                var userDistrictMapper = new UserDistrictMapper
                    {
                        DistrictId = district,
                        UserIds = listOfUsers,
                        WorkDate = DateTime.Today.Date
                    };
                objUserDistrictMapper.Add(userDistrictMapper);

                dataMapper.Delete(userDistrictMapper);
            }

            // var insertDataMapper = new UserDistrictDataMapper();

            // Deleting all 
            dataMapper.DeleteMappingByDate(DateTime.Today.Date);

            // Insert all
            dataMapper.InsertAll(objUserDistrictMapper);
        }

        // todo: Code a method that does not return anything, but receives data from NIF/NFF
        // todo: Code a method that returns the list of districts + number of matches from database
        public MatchManagerView GetMatchManagementListView()
        {
            List<District> districts = GetDistricts();

            // We need to check if the database has been updated
            var populateDataMapper = new PopulateDataMapper();
            var populate = populateDataMapper.GetDataBaseUpdated();
            //if (populate.End == null || populate.End > populate.Start)
            //{
            //    return null;
            //}

            if (populate.End == null)
            {
                return null;
            }


            UserManagerModels userManagerModels = new UserManagerModels();
            // Get all users
            List<UserProfile> userProfiles = new List<UserProfile>();


            foreach (MembershipUser u in Membership.GetAllUsers())
            {
                var userId = GetUserIdFromObject(u);

                var user = userManagerModels.GetUserProfile(u.UserName);

                UserProfile userProfile = new UserProfile
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserId = userId,
                        UserName = u.UserName,
                        Email = u.Email,
                        IsOnline = u.IsOnline,
                        LastLoginDate = u.LastLoginDate,
                        Districts = GetDistrictsForUser(userId)
                    };

                userProfiles.Add(userProfile);
            }

            Dictionary<District, List<Match>> dictionary = GetMatchesForDistrict(districts);

            var viewModel = new MatchManagerView
                {
                    UserProfiles = userProfiles,
                    Districts = districts,
                    DistrictMatches = dictionary
                };

            return viewModel;
        }

        private Guid GetUserIdFromObject(MembershipUser user)
        {
            return user.ProviderUserKey != null ? new Guid(user.ProviderUserKey.ToString()) : new Guid();
        }

        private List<District> GetDistrictsForUser(Guid userId)
        {
            var datamapper = new DistrictDataMapper();
            return datamapper.GetDistrictsByUserId(userId);
        }

        private List<District> GetDistricts()
        {
            var datamapper = new DistrictDataMapper();
            return new List<District>(datamapper.GetAll());
        }

        
    }
}