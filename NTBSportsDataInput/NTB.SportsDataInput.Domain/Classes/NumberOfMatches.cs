// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumberOfMatches.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The tools number of matches.
    /// </summary>
    public class NumberOfMatches
    {
        /// <summary>
        ///     Gets or sets the match date.
        /// </summary>
        public DateTime? MatchDate { get; set; }

        /// <summary>
        ///     Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the total number of matches.
        /// </summary>
        public int TotalMatches { get; set; }

        /// <summary>
        ///     Gets or sets the name of the sport.
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }
    }
}