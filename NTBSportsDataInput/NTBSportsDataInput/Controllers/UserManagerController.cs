﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NTB.SportsDataInput.Application.DataMappers.WorkRoles;
using NTB.SportsDataInput.Application.Models;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class UserManagerController : Controller
    {
        //
        // GET: /UserManager/

        public ActionResult Index()
        {
            var userManagerModel = new UserManagerModels();
            
            var userProfiles = userManagerModel.GetUserProfileView();

            return View(userProfiles);
        }

        //
        // GET: /UserManager/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /UserManager/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /UserManager/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var models = new UserManagerModels();
                models.InsertUser(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
 
        //
        // GET: /UserManager/Edit/abcdef
 
        public ActionResult Edit(string id)
        {
            var models = new UserManagerModels();
            var userProfile = models.GetUserProfileByUserId(new Guid(id));
            return View(userProfile);
        }

        //
        // POST: /UserManager/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                var models = new UserManagerModels();
                models.UpdateUser(collection, new Guid(id));

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /UserManager/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /UserManager/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult RemoveAllUsers()
        {
            foreach (MembershipUser u in Membership.GetAllUsers())
            {
                Membership.DeleteUser(u.UserName, true);
            }

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public ActionResult SetWorkRoles(FormCollection collection)
        {
            string[] arrayWorkRoles = collection["workrole[]"].Split(',');
            string[] arrayUserIds = collection["userid[]"].Split(',');

            var datamapper = new WorkRoleDataMapper();

            // First we clean all the current work roles
            datamapper.DeleteAll();
            int counter = 0;
            foreach (string stringWorkRole in arrayWorkRoles)
            {
                WorkRole workRole = new WorkRole
                    {
                        Id = Convert.ToInt32(stringWorkRole)
                    };
                
                datamapper.UserId = new Guid(arrayUserIds[counter]);

                datamapper.InsertOne(workRole);
                counter++;
            }



            return RedirectToAction("Index", "UserManager");
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

    }
}
