﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInput.Domain.Classes
{
    public class User
    {
        [Microsoft.Build.Framework.Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(150)]
        [Display(Name="Email address: ")]
        public string Email { get; set; }

        [Microsoft.Build.Framework.Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Password: ")]
        public string Password { get; set; }

    }
}
