﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.SMS.SMSGateway;

namespace NTB.SportsDataInput.SMS
{
    public class SmsGateway
    {
        public SmsGateway()
        {
            // Setting up the client
            SMSGateway_v1_0Client client = new SMSGateway_v1_0Client();   

            if (client.ClientCredentials != null)
            {
                client.ClientCredentials.UserName.UserName = "test";
                client.ClientCredentials.UserName.Password = "foo";
            }

            settings settings = new settings();

            message message = new message
                {
                    clientReference = "foo",
                    content = "Dette er en test",
                    price = 0,
                    priceSpecified = false,
                    recipient = "+4745035715",
                    settings = settings
                };

            var messages = new message[]{message};

            var request = new request
                {
                    message = messages,
                    password = "1234",
                    serviceId = 1234,
                    username = "1234"
                };
            
            var response = client.sendMessages(request);

            foreach (var foo in response.messageStatus)
            {
                if (foo.statusCode == 1)
                {
                    // Message was sent perfectly!
                }
            }
        }
    }
}
