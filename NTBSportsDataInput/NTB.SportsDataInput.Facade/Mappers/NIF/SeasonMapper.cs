﻿using Glue;
using NTB.SportsDataInput.Common;
using NTB.SportsDataInput.Domain.Classes;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Facade.Mappers.NIF
{
    public class SeasonMapper : BaseMapper<Services.NIF.NIFProdService.Season, Domain.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<Services.NIF.NIFProdService.Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x=>x.SeasonId, y=>y.SeasonId);
            mapper.Relate(x => x.SeasonName, y => y.SeasonName);
            mapper.Relate(x => x.FromDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.ToDate, y => y.SeasonEndDate);
        }
    }
}