// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Districts.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The districts.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The districts.
    /// </summary>
    public class District
    {
        /// <summary>
        /// Gets or sets the district id.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the secondary district Id - mostly soccer ID
        /// </summary>
        public int? SecondaryDistrictId { get; set; }

        /// <summary>
        /// Gets or sets the district name.
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether selected.
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        ///     Gets or sets the sport id related to this district
        /// </summary>
        public int? SportId { get; set; }
    }
}