﻿namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    interface ISportDataMapper
    {
        int GetSportIdBySeasonId(int seasonId);
        int GetSportIdFromOrgId(int orgId);
    }
}
