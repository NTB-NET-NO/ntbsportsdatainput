﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    public interface ITeamDataMapper
    {
        void DeleteTeamContactMap(int teamId, int contactId);
        void InsertTeamContactMap(int teamId, int contactId);
        
    }
}
