﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInput.Domain.Classes;

namespace NTB.SportsDataInput.Application.Interfaces.DataMappers
{
    public interface IContactPersonDataMapper
    {
        List<ContactPerson> GetContactPersonByTeamId(int teamId);
        void ActivateDeactivateContactPerson(int personId);
        void DeleteContactTeamMap(int teamId, int contactId);
        void InsertContactTeamMap(int teamId, int contactId);
    }
}
