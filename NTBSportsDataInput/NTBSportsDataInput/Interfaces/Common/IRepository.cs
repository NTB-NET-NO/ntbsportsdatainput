﻿using System.Collections.Generic;
using System.Linq;

namespace NTB.SportsDataInput.Application.Interfaces.Common
{
    public interface IRepository<T>
    {
        int InsertOne(T domainobject);
        void InsertAll(List<T> domainobject);
        void Update(T domainobject);
        void Delete(T domainobject);
        IQueryable<T> GetAll();
        T Get(int id);
    }
}
