﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInput.Services.NIF.NIFProdService;

namespace NTB.SportsDataInput.Services.NIF.Utilities
{
    class TournamentSortComparer : IComparer<Tournament>
    {
        public int Compare(Tournament x, Tournament y)
        {
            return String.Compare(x.TournamentName, y.TournamentName, StringComparison.OrdinalIgnoreCase);
        }
    }
}
