namespace NTB.SportsDataInput.Domain.Classes
{
    public class MatchComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
    }
}