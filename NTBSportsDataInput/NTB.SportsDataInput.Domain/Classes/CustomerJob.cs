// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerJob.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The customer jobs.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The customer jobs.
    /// </summary>
    public class CustomerJob
    {
        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the job status.
        /// </summary>
        public int JobStatus { get; set; }

        /// <summary>
        /// Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        /// Gets or sets if the customer is to receive schedule 
        /// </summary>
        public int Schedule { get; set; }

        /// <summary>
        ///     Gets or sets the Season Id
        /// </summary>
        public int SeasonId { get; set; }
    }
}