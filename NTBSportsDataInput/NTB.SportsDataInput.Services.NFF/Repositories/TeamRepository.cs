﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataInput.Services.NFF.Interfaces;
using NTB.SportsDataInput.Services.NFF.NFFProdService;

namespace NTB.SportsDataInput.Services.NFF.Repositories
{
    public class TeamRepository : IRepository<Team>, IDisposable , ITeamDataMapper
    {
        private readonly OrganizationServiceClient _serviceClient = new OrganizationServiceClient();

        public TeamRepository()
        {
            // Setting up the OrganizationClient
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
        }
        public int InsertOne(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Team> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Team> GetAll()
        {
            throw new NotImplementedException();
        }

        public Team Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Team> GetTeamsByMunicipality(int municipalityId)
        {
            return new List<Team>(_serviceClient.GetTeamsByMunicipality(municipalityId, null));
        }

        public List<OrganizationPerson> GetTeamPersons(int teamId)
        {
            var teamInfo = _serviceClient.GetTeam(teamId, true);

            if (teamInfo.TeamPersons == null)
            {
                return new List<OrganizationPerson>();
            }

            return teamInfo.TeamPersons.Select(teamPerson => new OrganizationPerson
                {
                    TeamId = teamInfo.TeamId, 
                    TeamName = teamInfo.TeamName, 
                    FirstName = teamPerson.FirstName, 
                    SurName = teamPerson.SurName, 
                    RoleId = teamPerson.RoleId, 
                    RoleName = teamPerson.RoleName, 
                    Email = teamPerson.Email, 
                    MobilePhone = teamPerson.MobilePhone, 
                    PersonId = teamPerson.PersonId
                }).ToList();
        }
    }
}
