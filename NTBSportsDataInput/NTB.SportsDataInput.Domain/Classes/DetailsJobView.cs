// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetailsJobView.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsDataInput.Domain.Classes
{
    /// <summary>
    /// The detail job view model.
    /// </summary>
    public class DetailsJobView
    {
        /// <summary>
        /// Gets or sets the Regions
        /// </summary>
        public List<District> Districts { get; set; }

        /// <summary>
        /// Gets or sets the counties.
        /// </summary>
        public List<Municipality> Municipalities { get; set; }

        /// <summary>
        /// Gets or sets the selected tournaments.
        /// </summary>
        public List<Tournament> Tournaments { get; set; }

        /// <summary>
        /// Gets or sets the selected municipalities
        /// </summary>
        public List<Municipality> SelectedMunicipalities { get; set; }

        /// <summary>
        ///     Gets or sets a boolean value for inserted age category.
        /// </summary>
        public bool InsertedAgeCategory { get; set; }
    }
}