﻿using System.Web.Mvc;
using log4net;

namespace NTB.SportsDataInput.Application.Controllers
{
    public class HomeController : Controller
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(HomeController));

        public ActionResult Index()
        {
            ViewBag.Message = "SportsData Input";

            return View();
        }

        public ActionResult Help()
        {
            ViewBag.Message = "SportsData Input - Hjelp";
            return View();
        }
    }
}
